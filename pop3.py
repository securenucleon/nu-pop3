# Pop3 honeypot server , Detects and log connections
# Copyright ShieldLock(C) 2015 

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation version 2.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver
from twisted.internet import reactor
import os, os.path
import time, uuid
import ConfigParser
from twisted.enterprise import adbapi
from twisted.python import log
import logging
import sys

import unicodedata

class FakePop3Session(LineReceiver):
        def __init__(self):
    	        self.name = None

                self.delimiter = '\n'
                self.session = str(uuid.uuid1()) # Dirty. todo. 
                self.state='AUTHUSER'

        def connectionMade(self):
                self.transport.write('+OK QPOP (version 2.2) at 127.0.0.1 starting.\r\n')
                logger.debug(u'%s|New|%s' % (self.session,self.transport.getHost()))
                self.state = 'AUTHUSER'

        def connectionLost(self, reason):
                logger.debug(u'%s|Closed' % self.session)

        def lineReceived(self, line):
                line = line.replace(b"\r", b"") # Remove unneccessary chars
                command = line.strip().lower()
                if (command in ['quit', 'exit']):
                        self.transport.write('+OK Pop server at %s signing off.\r\n' % 'bilbi.odo.gov')
	                logger.debug(u'%s|Quit' % self.session)

                        self.transport.loseConnection()
                elif (command.startswith('capa')):
                        logger.debug(u'%s|Capa|%s|%s' % (self.session, self.transport.getHost(),command))
                        self.transport.write('+OK Capability list follows\r\n')
                        self.transport.write('TOP\r\n')
                        self.transport.write('APOP\r\n')
                        self.transport.write('USER\r\n')
                        self.transport.write('PASS\r\n')
                        self.transport.write('STAT\r\n')
                        self.transport.write('LIST\r\n')
                        self.transport.write('RETR\r\n')
                        self.transport.write('DELE\r\n')
                        self.transport.write('RSET\r\n')
                        self.transport.write('.\r\n')
                else:
                        getattr(self, 'pop3_' + self.state)(command)
        def pop3_AUTHUSER(self, command):
                if (command.startswith('user')):
                        logger.debug(u'%s|User|%s|%s' % (self.session, self.transport.getHost(),command))
                        self.transport.write('+OK Password required for %s.\r\n' % command[4:].strip())
                        self.state = 'AUTHPASS'
                elif (command.startswith('apop') and len(command) > 15):
                        logger.debug(u'%s|apop|%s|%s' % (self.session, self.transport.getHost(),command))
                        self.transport.write('+OK')
                        self.state = 'META'
                else:
			try :
                        	logger.error(u'%s|Error|%s|%s' % (self.session, self.transport.getHost(),str(cmd)))
			except :
                                logger.error(u'%s|Error|%s' % (self.session, self.transport.getHost()))

                        self.transport.write('-ERR Authentication required.\r\n')

        def pop3_AUTHPASS(self, command):
                if (command.startswith('pass')):
                        logger.debug(u'%s|PASS|%s|%s' % (self.session, self.transport.getHost(),command))
                        self.transport.write('+OK User has 3 messages (198274 octets).\r\n')
                        self.state = 'META'
                else:
                        logger.error(u'%s|Error|%s|%s' % (self.session, self.transport.getHost(),command))
                        self.transport.write('-ERR Password required.\r\n')

        def pop3_META(self, command):

                if (command.startswith('retr') or command.startswith('top')):
                                self.transport.write('-ERR Requested mail does not exist.\r\n')
	                        logger.debug(u'%s|RETR|%s|%s' % (self.session, self.transport.getHost(),command))

                elif (command.startswith('stat')):
                                self.transport.write('-ERR Requested mail does not exist.\r\n')
                                logger.debug(u'%s|STAT|%s|%s' % (self.session, self.transport.getHost(),command))

                elif (command.startswith('list')):
                       self.transport.write('+OK 3 messages:\r\n')               
                       self.transport.write('1 12320\r\n')
                       self.transport.write('2 23603\r\n')
                       self.transport.write('3 1240\r\n')
                       self.transport.write('.\r\n')
                       logger.debug(u'%s|LIST|%s|%s' % (self.session, self.transport.getHost(),command))

                elif (command.startswith('dele')):
                        self.transport.write('+OK Message deleted\r\n')
                        logger.debug(u'%s|DEL|%s|%s' % (self.session, self.transport.getHost(),command))

                elif (command.startswith('rset')):
                        self.transport.write('+OK Reset state\r\n')
                        self.state = 'AUTHPASS'
                        logger.debug(u'%s|RSET|%s|%s' % (self.session, self.transport.getHost(),command))

                else:
                        logger.error(u'%s|Error|%s' % (self.session, self.transport.getHost()))
                        self.transport.write('-ERR Invalid command specified.\r\n')

class POPFactory(Factory):

    def buildProtocol(self, addr):
        return FakePop3Session()


config = ConfigParser.ConfigParser()
config.read('pop3.cfg')
serverport = config.getint('General', 'service_port')
logfile=config.get('General','logfile')

logger  = logging.getLogger('POP3Server')
logger.addHandler(logging.StreamHandler(sys.stderr))
logger.addHandler(logging.FileHandler(logfile))
for h in logger.handlers:
    h.setFormatter(logging.Formatter(fmt='%(asctime)s|[%(name)s.%(levelname)s %(lineno)d]:| %(message)s'))

logger.setLevel(logging.DEBUG)
logger.debug(u'Starting Server')

reactor.listenTCP(serverport, POPFactory())
reactor.run()
